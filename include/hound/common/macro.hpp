//
// Created by brian on 11/22/23.
//

#ifndef HOUND_MACROS_HPP
#define HOUND_MACROS_HPP

#include <iostream>
#include <hound/common/dbg.hpp>

namespace hd::macro {
static std::mutex coutMutex;

template <typename ...T>
static void printL(T ...args) {
  std::lock_guard lock(coutMutex);
  ((std::cout << args), ...);
}
} // namespace hd::macro

#pragma region 常量宏
#ifndef XXX_PADSIZE
#define IP4_PADSIZE  60
#define TCP_PADSIZE  60
#define UDP_PADSIZE  8
#define XXX_PADSIZE
#endif// XXX_PADSIZE
#pragma endregion 常量宏

#pragma region 功能性宏


#ifndef HD_ANSI_COLOR
#define HD_ANSI_COLOR
#define RED(x)     "\033[31;1m" x "\033[0m"
#define GREEN(x)   "\033[32;1m" x "\033[0m"
#define YELLOW(x)  "\033[33;1m" x "\033[0m"
#define BLUE(x)    "\033[34;1m" x "\033[0m"
#define CYAN(x)    "\033[36;1m" x "\033[0m"
#endif //HD_ANSI_COLOR

#ifndef hd_info
#define hd_info(...)       			\
do {                     				\
hd::macro::printL(__VA_ARGS__); \
} while (false)
#endif//-hd_info


/// 仅在开发阶段作为调试使用
#if defined(HD_DEBUG)
#ifndef hd_debug
#define hd_debug(...)  dbg(__VA_ARGS__)
#endif

#ifndef hd_line
#define hd_line(...)   dbg(__VA_ARGS__)
#endif//-hd_line

#else//- not HD_DEBUG
#define hd_debug(...)

#ifndef hd_line
#define hd_line(...)       			      \
do {                     				      \
hd::macro::printL(__VA_ARGS__, "\n"); \
} while (false)
#endif
#endif//- hd_debug

#pragma endregion 功能性宏

#endif //HOUND_MACROS_HPP
